package org.spigotmc.lidle.commands.subcommands.savagefactions.game;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;

public class LeaveCommand extends FCommand {

    LeaveCommand() {
        super();
        this.aliases.add("leave");

    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {

    }
}
