package org.spigotmc.lidle.commands.subcommands.savagefactions.party;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyInviteEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class InviteCommand extends FCommand {

    InviteCommand() {
        super();
        this.aliases.add("invite");
        this.permission = FactionsCommandPermissionModel.Node.PARTY_INVITE;
        this.requiredArgs.add("player name");

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        //Player not found
        FPlayer target = this.argAsBestFPlayerMatch(0);
        if (target == null) {
            return;
        }

        boolean inParty = Party.inParty(target);
        boolean inSameFaction = target.getFaction() == myFaction;

        //If the player isn't in a party and is in the same faction
        if(!inParty && inSameFaction) {
            //If a current party exists for the faction
            if(Party.doesPartyExistForFaction(myFaction)) {
                //Create a party invite and invoke the invite event
                PartyInviteEvent event = new PartyInviteEvent(fme, target);
                Events.fire(event);
            } else {
                //Party doesn't exists for the specified faction
                Message message = Message.MessageBuilder.builder()
                        .withMessage(Messages.PARTY_DOESNT_EXIST)
                        .withMessageType(MessageType.WARNING)
                        .build();

                message.sendMessage(me);
            }
        }

        //The player is already in a party or not in the same faction
        else {
            String msg = "Could not invite player to party";

            if(!inSameFaction) {
                msg = Messages.PLAYER_NOT_IN_SAME_FACTION;
            } else if(inParty) {
                msg = Messages.PLAYER_IN_PARTY;
            }

            Message message = Message.MessageBuilder.builder()
                    .withMessage(msg)
                    .withMessageType(MessageType.WARNING)
                    .build();

            message.sendMessage(me);
        }
    }
}
