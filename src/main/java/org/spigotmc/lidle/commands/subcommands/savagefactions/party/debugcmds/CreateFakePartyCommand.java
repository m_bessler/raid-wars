package org.spigotmc.lidle.commands.subcommands.savagefactions.party.debugcmds;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;

public class CreateFakePartyCommand extends FCommand {

    public CreateFakePartyCommand() {
        super();
        this.aliases.add("fakeparty");
        this.aliases.add("fp");

        this.permission = FactionsCommandPermissionModel.Node.PARTY_CREATE;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {

    }
}
