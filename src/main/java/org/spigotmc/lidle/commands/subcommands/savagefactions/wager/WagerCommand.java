package org.spigotmc.lidle.commands.subcommands.savagefactions.wager;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;

public class WagerCommand extends FCommand {

    public WagerCommand() {
        super();
        this.aliases.add("wager");
        this.permission = FactionsCommandPermissionModel.Node.WAGER;

        this.addSubCommand(new AcceptCommand());
        this.addSubCommand(new DeclineCommand());
        this.addSubCommand(new InviteCommand());
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {

    }
}
