package org.spigotmc.lidle.commands.subcommands;

import org.spigotmc.lidle.annotations.commands.CommandDataAnnotation;
import org.spigotmc.lidle.permissions.CommandPermissionModel;

@CommandDataAnnotation(getName = "SetChest",
        getDescription = "Sets the chest the factions must interact with",
        getArgs = "")
public class SetChestCommand extends AbstractSubCommand {

    public SetChestCommand(String cmd) {
        super(cmd);
    }

    @Override
    protected boolean checkPermission(CommandPermissionModel permissionModel) {
        return permissionModel.mayUseSetChest();
    }

    @Override
    protected String getUsage() {
        return null;
    }

    @Override
    protected boolean execute() {
        return false;
    }
}
