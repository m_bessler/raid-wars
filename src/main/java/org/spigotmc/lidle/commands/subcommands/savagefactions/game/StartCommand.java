package org.spigotmc.lidle.commands.subcommands.savagefactions.game;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.game.GameEvent;
import org.spigotmc.lidle.events.subevents.savagefactions.game.GameStartEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.game.GameHelper;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.generics.State;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class StartCommand extends FCommand {

    StartCommand() {
        super();
        this.aliases.add("start");
        this.aliases.add("begin");

        this.permission = FactionsCommandPermissionModel.Node.GAME_START;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = true;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        //Party must exist
        if(!Party.doesPartyExistForFaction(myFaction)) {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.PARTY_DOESNT_EXIST)
                    .withMessageType(MessageType.WARNING)
                    .build();
            message.sendMessage(me);
            return;
        }

        //Must be in a party
        if(!Party.inParty(fme)) {
            Message message = Messages.FormattedMessage.notInParty(me);
            message.sendMessage(me);
            return;
        }

        //Get the existing party for the faction
        Party party = Party.getPartyForFaction(myFaction);

        //Team must exist for the party
        if(!Team.doesTeamExistForParty(party)) {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.TEAM_DOESNT_EXIST)
                    .withMessageType(MessageType.WARNING)
                    .build();
            message.sendMessage(me);
            return;
        }

        //Used to for helping to check the existing game's state (if the game exists)
        GameHelper gameHelper = new GameHelper(party);
        State startGameState = gameHelper.getStartGameState();

        //If the requirements to start the game are met, fire the game start event
        if(startGameState == State.ALLOW) {
            GameStartEvent event = new GameStartEvent(fme);
            Events.fire(event);
        }
        //Doesn't meet requirements to start game
        else {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.GAME_START_CONDITION_NOT_MET)
                    .withMessageType(MessageType.WARNING)
                    .build();
            message.sendMessage(me);
            return;
        }

    }
}
