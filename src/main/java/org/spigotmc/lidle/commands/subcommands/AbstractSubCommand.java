package org.spigotmc.lidle.commands.subcommands;

import org.spigotmc.lidle.RaidWars;
import org.spigotmc.lidle.commands.AbstractCommandArgument;
import org.spigotmc.lidle.permissions.CommandPermissionModel;

import java.util.List;

/*Authored by: Matt Bessler 8/11/2018*/
public abstract class AbstractSubCommand extends AbstractCommandArgument {

    private static RaidWars pl = RaidWars.inst();

    private CommandPermissionModel permissionModel;

    public AbstractSubCommand(String cmd) {
        super(cmd);
    }

    public AbstractSubCommand(String cmd, String... aliases) {
        super(cmd, aliases);
    }

    public AbstractSubCommand(String cmd, List<String> aliases) {
        super(cmd, aliases);
    }

    @Override
    protected boolean hasPermission(){
        permissionModel = new CommandPermissionModel(pl, getCommandSender());
        return checkPermission(permissionModel);
    }

    protected abstract boolean checkPermission(CommandPermissionModel permissionModel);
}
