package org.spigotmc.lidle.commands.subcommands.savagefactions.party;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyEvent;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyKickEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class KickCommand extends FCommand {

    KickCommand() {
        super();
        this.aliases.add("kick");
        this.permission = FactionsCommandPermissionModel.Node.PARTY_KICK;
        this.requiredArgs.add("player name");

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        //The player the party leader wishes to kick
        FPlayer target = this.argAsBestFPlayerMatch(0);
        if (target == null) {
            return;
        }

        if(fme == target) {
            Message.MessageBuilder.builder()
                    .withMessage("You cannot kick yourself!")
                    .withMessageType(MessageType.INFO)
                    .build()
                    .sendMessage(me);
            return;
        }

        //Checking to make sure they are in a party
        if(Party.inParty(fme)) {
            Party party = Party.getPartyForFaction(myFaction);

            //Checking if the player is the party leader
            if(party.isPartyLeader(fme)) {
                PartyEvent event = new PartyKickEvent(fme, target);
                Events.fire(event);
            } else {
                Message message = Message.MessageBuilder.builder()
                        .withMessage(Messages.NOT_PARTY_LEADER)
                        .withMessageType(MessageType.WARNING)
                        .build();

                message.sendMessage(me);
            }
        } else {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.NOT_IN_PARTY)
                    .withMessageType(MessageType.WARNING)
                    .build();

            message.sendMessage(me);
        }
    }
}
