package org.spigotmc.lidle.commands.subcommands.savagefactions.party;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class DisbandCommand extends FCommand {

    DisbandCommand() {
        super();
        this.aliases.add("disband");
        this.permission = FactionsCommandPermissionModel.Node.PARTY_DISBAND;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        //Disband the party only if they are in a party and are the party leader
        if(Party.inParty(fme)) {
            Party party = Party.getPartyForFaction(myFaction);

            //Checking if the player is the party leader
            if(party.isPartyLeader(fme)) {
                PartyEvent event = new PartyEvent(fme, PartyEvent.EventType.DISBAND);
                Events.fire(event);
            } else {
                Message message = Message.MessageBuilder.builder()
                        .withMessage(Messages.NOT_PARTY_LEADER)
                        .withMessageType(MessageType.WARNING)
                        .build();

                message.sendMessage(me);
            }
        } else {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.NOT_IN_PARTY)
                    .withMessageType(MessageType.WARNING)
                    .build();

            message.sendMessage(me);
        }
    }
}
