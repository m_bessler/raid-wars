package org.spigotmc.lidle.commands.subcommands.savagefactions.party.debugcmds;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.utils.fakes.raidwars.FakeRaidPlayer;
import org.spigotmc.lidle.utils.fakes.raidwars.FakePlayer;
import org.spigotmc.lidle.raidwars.party.Party;

public class ForceJoinCommand extends FCommand {

    public ForceJoinCommand() {
        super();
        this.aliases.add("forcejoin");
        this.aliases.add("fj");

        this.permission = FactionsCommandPermissionModel.Node.PARTY_ACTION;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        Party party = Party.getPartyForFaction(fme.getFaction());
        party.forceAddPartyMember(new FakeRaidPlayer(new FakePlayer()));
    }
}
