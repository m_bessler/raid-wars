package org.spigotmc.lidle.commands.subcommands.savagefactions.party;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class LeaveCommand extends FCommand {

    LeaveCommand() {
        super();
        this.aliases.add("leave");
        this.permission = FactionsCommandPermissionModel.Node.PARTY_LEAVE;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        //Checking if player is in an existing party
        if(Party.inParty(fme)) {
            PartyEvent event = new PartyEvent(fme, PartyEvent.EventType.LEAVE);
            Events.fire(event);
        } else {
            //Notify player they are not in a party
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.NOT_IN_PARTY)
                    .withMessageType(MessageType.WARNING)
                    .build();
            message.sendMessage(me);
        }
    }
}
