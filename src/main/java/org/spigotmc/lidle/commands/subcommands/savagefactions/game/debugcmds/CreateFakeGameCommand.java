package org.spigotmc.lidle.commands.subcommands.savagefactions.game.debugcmds;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.game.GameInviteEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.fakes.raidwars.FakePlayer;
import org.spigotmc.lidle.utils.fakes.raidwars.FakeRaidPlayer;

import java.util.UUID;

public class CreateFakeGameCommand extends FCommand {

    public CreateFakeGameCommand() {
        super();
        this.aliases.add("fakegame");
        this.aliases.add("fg");

        this.permission = FactionsCommandPermissionModel.Node.GAME_ACTION;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = true;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        UUID uuid = UUID.fromString("26620f57-a8f0-4898-a3cc-49bf5c88d1b1");
        FakePlayer fakeAmziie = new FakePlayer("Amziie", uuid);
        RaidPlayer amziie = new FakeRaidPlayer(fakeAmziie);

        Faction faction = Factions.getInstance().getFactionById("2");
        Party.registerNewParty(faction, amziie);

        //They invite my faction
        Party partyInvited = Party.getPartyForFaction(myFaction);
        Team teamInvited = Team.getOrRegisterTeamForParty(partyInvited);

        //Amziies party which invited me
        Party partyWhoInvited = Party.getPartyForFaction(faction);
        Team teamWhoInvited = Team.getOrRegisterTeamForParty(partyWhoInvited);

        Game gameInvitedTo = new Game(teamWhoInvited);
        GameInviteEvent event = new GameInviteEvent(fme, teamWhoInvited, teamInvited, gameInvitedTo);
        Events.fire(event);
    }
}
