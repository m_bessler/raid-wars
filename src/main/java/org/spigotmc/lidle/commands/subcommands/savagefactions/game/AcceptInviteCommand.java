package org.spigotmc.lidle.commands.subcommands.savagefactions.game;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.game.GameInviteActionEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.invites.GameInvite;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.generics.Action;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class AcceptInviteCommand extends FCommand {

    AcceptInviteCommand() {
        super();
        this.aliases.add("accept");
        this.aliases.add("join");

        this.permission = FactionsCommandPermissionModel.Node.PARTY_ACTION;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        RaidPlayer raidPlayer = RaidPlayer.getOrCreateRaidPlayer(fme);
        //Validating that a party exists for their faction
        if(!Party.doesPartyExistForFaction(myFaction)) {
            //Revoking invite so they can no longer use it
            raidPlayer.revokeInvite();

            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.PARTY_DOESNT_EXIST)
                    .withMessageType(MessageType.WARNING)
                    .build();

            raidPlayer.sendMessage(message);
            return;
        }
        Team team = Team.getOrRegisterTeamForParty(raidPlayer.getParty());
        //Validating that they have an existing invite
        if(!team.hasInvite()) {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.INVITE_NO_LONGER_EXISTS)
                    .withMessageType(MessageType.WARNING)
                    .build();

            raidPlayer.sendMessage(message);
            return;
        }

        //Player has a valid invite and a party exists for their faction
        //Fire the PartyInviteAction event!
        GameInvite invite = team.getInvite();

        GameInviteActionEvent event = new GameInviteActionEvent(fme, team, invite, Action.ACCEPT);
        Events.fire(event);
    }
}
