package org.spigotmc.lidle.commands.subcommands.savagefactions.game;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.game.GameInviteEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.utils.event.Events;

public class InviteCommand extends FCommand {

    InviteCommand() {
        super();
        this.aliases.add("invite");
        this.aliases.add("inv");

        this.requiredArgs.add("faction tag");

        this.permission = FactionsCommandPermissionModel.Node.SAVAGE_RAID_WARS;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeModerator = true;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        //The faction they want to invite to the team
        Faction invitedFaction = this.argAsFaction(0);
        if (invitedFaction == null) {
            return;
        }

        if(!Party.doesPartyExistForFaction(myFaction)) {
            //Notify the invitee faction they need to put together a party
            return;
        }

        if(Party.doesPartyExistForFaction(invitedFaction)) {
            //Notify both parties that the invited factions needs a party
            return;
        }

        //Both parties exist for the faction
        //Invoke the game invite event
        Party partyInvited = Party.getPartyForFaction(invitedFaction);
        Team teamInvited = Team.getOrRegisterTeamForParty(partyInvited);

        Party partyWhoInvited = Party.getPartyForFaction(myFaction);
        Team teamWhoInvited = Team.getOrRegisterTeamForParty(partyWhoInvited);

        Game gameInvitedTo = new Game(teamWhoInvited);
        GameInviteEvent event = new GameInviteEvent(fme, teamWhoInvited, teamInvited, gameInvitedTo);
        Events.fire(event);
    }
}
