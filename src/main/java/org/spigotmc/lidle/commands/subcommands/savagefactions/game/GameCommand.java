package org.spigotmc.lidle.commands.subcommands.savagefactions.game;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.Debug;
import org.spigotmc.lidle.commands.subcommands.savagefactions.game.debugcmds.CreateFakeGameCommand;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;

public class GameCommand extends FCommand {

    public GameCommand() {
        super();
        this.aliases.add("game");

        this.permission = FactionsCommandPermissionModel.Node.GAME;

        this.addSubCommand(new AcceptInviteCommand());
        this.addSubCommand(new DenyInviteCommand());
        this.addSubCommand(new InviteCommand());
        this.addSubCommand(new LeaveCommand());
        this.addSubCommand(new ReadyCommand());
        this.addSubCommand(new StartCommand());
        if(Debug.isEnabled()) {
            this.addSubCommand(new CreateFakeGameCommand());
        }
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {

    }
}
