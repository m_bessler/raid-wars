package org.spigotmc.lidle.commands.subcommands.savagefactions.party;

import com.massivecraft.factions.cmd.FCommand;
import com.massivecraft.factions.zcore.util.TL;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyInviteActionEvent;
import org.spigotmc.lidle.permissions.FactionsCommandPermissionModel;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.invites.PartyInvite;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.utils.event.Events;
import org.spigotmc.lidle.utils.generics.Action;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messages;

public class JoinCommand extends FCommand {

    JoinCommand() {
        super();
        this.aliases.add("join");
        this.aliases.add("accept");

        this.permission = FactionsCommandPermissionModel.Node.PARTY_ACTION;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeColeader = false;
        senderMustBeAdmin = false;
    }

    @Override
    public TL getUsageTranslation() {
        return null;
    }

    @Override
    public void perform() {
        RaidPlayer raidPlayer = RaidPlayer.getOrCreateRaidPlayer(fme);
        PartyInvite invite = raidPlayer.getInvite();

        //Making sure the player isn't already in a party
        if(!Party.inParty(fme)) {
            if(invite != null && invite.doesExist()) {
                if(!invite.isAccepted()) {
                    PartyInviteActionEvent event = new PartyInviteActionEvent(fme, invite, Action.ACCEPT);
                    Events.fire(event);
                }
            } else {
                Message message = Message.MessageBuilder.builder()
                        .withMessage(Messages.PARTY_INVITE_DOESNT_EXIST)
                        .withMessageType(MessageType.WARNING)
                        .build();

                message.sendMessage(raidPlayer.getPlayer());
            }
        } else {
            Message message = Message.MessageBuilder.builder()
                    .withMessage(Messages.ALREADY_IN_PARTY)
                    .withMessageType(MessageType.WARNING)
                    .build();
            message.sendMessage(me);
        }
    }
}
