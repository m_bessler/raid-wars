package org.spigotmc.lidle.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

/*Authored by: Matt Bessler 8/10/2018*/
public class RaidWarsCommands implements CommandExecutor {

    private List<AbstractCommandArgument> subcmds;

    public RaidWarsCommands() {
        this.subcmds = new ArrayList();
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length > 0) {
            AbstractCommandArgument cmdArg = getSubCommand(args[0]);

            if(cmdArg != null) {
                List<String> subCmdArgs = getSubCommandArguments(args);
                cmdArg.execute(sender, subCmdArgs);
                return true;
            } else {
                showAllUsages(sender);
            }
        } else {
            showAllUsages(sender);
        }

        return false;
    }

    private AbstractCommandArgument getSubCommand(String label) {
        for (AbstractCommandArgument cmd : this.subcmds) {
            if (cmd.isCommand(label)) {
                return cmd;
            }
        }

        return null;
    }

    private List<String> getSubCommandArguments(String[] args) {
        List<String> subCmdArgs = new ArrayList(Arrays.asList(args));

        if (!subCmdArgs.isEmpty()) {
            subCmdArgs.remove(0);
        }

        return subCmdArgs;
    }

    private void showAllUsages(CommandSender sender) {
        List<String> usages = getAllSubCommandUsages();
        for(String usage : usages) {
            Message.MessageBuilder.builder()
                    .withMessage(usage)
                    .withMessageType(MessageType.INFO)
                    .build()
                    .sendMessage(sender);
        }
    }

    private List<String> getAllSubCommandUsages(){
        List<String> usages = new ArrayList<>();
        String commandPrefix = "/rw";

        StringJoiner joiner;
        for(AbstractCommandArgument command : subcmds) {
            for(String usage : command.getUsages()) {
                joiner = new StringJoiner(" ");
                joiner.add(commandPrefix);
                joiner.add(usage);
                usages.add(joiner.toString());
            }
        }

        return usages;
    }

}
