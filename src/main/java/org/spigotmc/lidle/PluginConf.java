package org.spigotmc.lidle;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/*Authored by: Matt Bessler 8/10/2018*/
public class PluginConf {

    private static PluginConf inst;

    private RaidWars pl;
    private YamlConfiguration configFile;

    protected PluginConf(){}

    public PluginConf(RaidWars pl){
        this.pl = pl;
        inst = this;
        init();
    }

    private void init() {
        loadConfigFiles();
    }

    private void loadConfigFiles(){
        File tmpConfigFile = new File(pl.getDataFolder(), "config.yml");

        if (!tmpConfigFile.exists()) {
            tmpConfigFile.getParentFile().mkdirs();
            pl.saveResource("config.yml", false);
        }

        configFile = new YamlConfiguration();
        try {
            configFile.load(tmpConfigFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public boolean getBroadcastGameStart(){
        return configFile.getBoolean(ConfNode.BROADCAST_GAME_START);
    }

    public String getBroadcastGameStartPrefix() { return configFile.getString(ConfNode.BROADCAST_GAME_START_PREFIX); }

    public int getMaxPartySize() {
        return configFile.getInt(ConfNode.MAX_PARTY_SIZE);
    }

    public int getMinPartySize() {
        return configFile.getInt(ConfNode.MIN_PARTY_SIZE);
    }

    public int getGameStartCountdownSeconds() { return configFile.getInt(ConfNode.GAME_START_COUNTDOWN_SECONDS); }

    protected void save(File configFile, FileConfiguration fileConfiguration) throws IOException {
        fileConfiguration.save(configFile);
    }

    public static PluginConf inst() {
        return inst;
    }

    //CONFIG NODES
    private static final class ConfNode {
        static final String BROADCAST_GAME_START = "broadcast-game";
        static final String BROADCAST_GAME_START_PREFIX =  "broadcast-game-prefix";
        static final String MAX_PARTY_SIZE = "max-party-size";
        static final String MIN_PARTY_SIZE = "min-party-size";
        static final String GAME_START_COUNTDOWN_SECONDS = "game-start-countdown-seconds";
    }
}
