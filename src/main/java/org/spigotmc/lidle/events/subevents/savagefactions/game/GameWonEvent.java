package org.spigotmc.lidle.events.subevents.savagefactions.game;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.team.Team;

public class GameWonEvent extends GameEvent {

    public GameWonEvent(RaidPlayer whoFired, EventType eventType) {
        super(whoFired, eventType);
    }

    public RaidPlayer getPlayerWhoWon() {
        return whoFired;
    }

    public Team getWinningTeam() {
        return getTeam();
    }

}
