package org.spigotmc.lidle.events.subevents.savagefactions.game;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.raidwars.invites.GameInvite;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.utils.generics.Action;
import org.spigotmc.lidle.utils.invites.InviteDispatcher;

/**
 * An event which models the data when a team accepts or denies
 * a game invite sent to them
 * */
public class GameInviteActionEvent extends GameEvent {

    private Action action;
    private InviteDispatcher inviteDispatcher;

    public GameInviteActionEvent(FPlayer whoFired, Team invitedTeam, GameInvite gameInvite, Action action) {
        super(whoFired, EventType.INVITE_ACTION);
        this.action = action;
        this.inviteDispatcher = new InviteDispatcher(gameInvite, invitedTeam);
    }

    public InviteDispatcher getInviteDispatcher() {
        return inviteDispatcher;
    }

    public Action getAction() {
        return action;
    }

}
