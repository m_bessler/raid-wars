package org.spigotmc.lidle.events.subevents.savagefactions.game;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.events.subevents.savagefactions.FactionsEvent;
import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.game.GameNotificationDispatcher;
import org.spigotmc.lidle.raidwars.team.Team;

public class GameEvent extends FactionsEvent {

    private Game game;
    private Team team;
    private EventType eventType;
    private GameNotificationDispatcher gameNotificationDispatcher;

    public GameEvent(FPlayer whoFired, EventType eventType) {
        super(whoFired);
        this.team = Team.getOrRegisterTeamForParty(this.whoFired.getParty());
        this.game = new Game(team);
        this.eventType = eventType;
        this.gameNotificationDispatcher = new GameNotificationDispatcher(game);
    }

    /**
     * @return the game which this event was invoked in
     * */
    public Game getGame() {
        return game;
    }

    /**
     * @return the team which invoked this event
     * */
    public Team getTeam() {
        return team;
    }

    /**
     * @return the event type which was invoked
     * */
    public EventType getEventType() {
        return eventType;
    }

    /**
     * @return the notification dispatcher for this game
     * */
    public GameNotificationDispatcher getGameNotificationDispatcher() {
        return gameNotificationDispatcher;
    }

    public enum EventType {
        INVITE,
        INVITE_ACTION,
        LEAVE,
        START
    }
}
