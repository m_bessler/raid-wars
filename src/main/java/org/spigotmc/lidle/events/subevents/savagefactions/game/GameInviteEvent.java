package org.spigotmc.lidle.events.subevents.savagefactions.game;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.team.Team;

public class GameInviteEvent extends GameEvent {

    private Team teamWhoInvited;
    private Game invitedGame;
    private Team invitedTeam;

    public GameInviteEvent(FPlayer whoFired, Team teamWhoInvited, Team invitedTeam, Game invitedGame) {
        super(whoFired, EventType.INVITE);
        this.teamWhoInvited = teamWhoInvited;
        this.invitedTeam = invitedTeam;
        this.invitedGame = invitedGame;
    }

    public Team getTeamWhoInvited() {
        return teamWhoInvited;
    }

    public Game getInvitedGame() {
        return invitedGame;
    }

    public Team getInvitedTeam() {
        return invitedTeam;
    }

}
