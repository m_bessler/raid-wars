package org.spigotmc.lidle.events.subevents.savagefactions.wager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.spigotmc.lidle.utils.event.EventListenerRegisterer;

public class WagerListener implements Listener {

    public WagerListener() {
        EventListenerRegisterer.addListener(this);
    }

    @EventHandler
    public void onWager(WagerEvent e) {
        WagerEvent.EventType eventType = e.getEventType();

        switch (eventType) {
            case ACCEPT:
                accept(e);
                break;
            case DECLINE:
                decline(e);
                break;
            case INVITE:
                invite(e);
                break;
            default:
                break;
        }
    }

    private void accept(WagerEvent e) {

    }

    private void decline(WagerEvent e) {

    }

    private void invite(WagerEvent e) {

    }

}
