package org.spigotmc.lidle.events.subevents.savagefactions.party;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.party.PartyNotificationDispatcher;

public class PartyKickEvent extends PartyEvent {

    private RaidPlayer playerToKick;

    public PartyKickEvent(FPlayer whoFired, FPlayer playerToKick) {
        super(whoFired, EventType.KICK);
        this.playerToKick = getAsRaidPlayer(playerToKick);
        setPartyNotificationDispatcher(new PartyNotificationDispatcher(getParty(), this.whoFired, this.playerToKick));
    }

    public RaidPlayer getPlayerToKick() {
        return playerToKick;
    }

}
