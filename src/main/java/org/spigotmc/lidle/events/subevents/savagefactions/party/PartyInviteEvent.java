package org.spigotmc.lidle.events.subevents.savagefactions.party;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.invites.PartyInvite;
import org.spigotmc.lidle.utils.invites.InviteDispatcher;

public class PartyInviteEvent extends PartyEvent {

    private PartyInvite partyInvite;
    private InviteDispatcher inviteDispatcher;

    public PartyInviteEvent(FPlayer whoFired, FPlayer invitedPlayer) {
        super(RaidPlayer.getOrCreateRaidPlayer(whoFired), EventType.INVITE);
        this.partyInvite = new PartyInvite(whoFired, getPlayer().getParty());
        this.inviteDispatcher = new InviteDispatcher(partyInvite, getAsRaidPlayer(invitedPlayer));
    }

    public PartyInvite getPartyInvite() {
        return partyInvite;
    }

    public InviteDispatcher getInviteDispatcher() {
        return inviteDispatcher;
    }

}
