package org.spigotmc.lidle.events.subevents.savagefactions.game;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.PluginConf;
import org.spigotmc.lidle.utils.Ticks;

public class GameStartEvent extends GameEvent {

    private Ticks ticks;

    public GameStartEvent(FPlayer whoFired) {
        super(whoFired, EventType.START);
        int seconds = PluginConf.inst().getGameStartCountdownSeconds();
        this.ticks = new Ticks(seconds);
    }

    public long getCountdownSeconds() {
        return ticks.getTicksAsSeconds();
    }

}
