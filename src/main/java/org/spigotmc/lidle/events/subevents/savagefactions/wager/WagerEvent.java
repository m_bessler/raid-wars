package org.spigotmc.lidle.events.subevents.savagefactions.wager;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.events.subevents.savagefactions.FactionsEvent;
import org.spigotmc.lidle.raidwars.team.Team;

public class WagerEvent extends FactionsEvent {

    private Team requestedTeam;
    private Team requestingTeam;
    private EventType eventType;

    public WagerEvent(FPlayer whoFired, Team team1, Team team2, EventType eventType) {
        super(whoFired);
        this.requestingTeam = team1;
        this.requestedTeam = team2;
        this.eventType = eventType;
    }

    public Team getRequestingTeam() {
        return requestingTeam;
    }

    public Team getRequestedTeam() {
        return requestedTeam;
    }

    public EventType getEventType() {
        return eventType;
    }

    public enum EventType {
        ACCEPT,
        DECLINE,
        INVITE
    }

}
