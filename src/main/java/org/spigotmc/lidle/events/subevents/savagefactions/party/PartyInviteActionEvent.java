package org.spigotmc.lidle.events.subevents.savagefactions.party;

import com.massivecraft.factions.FPlayer;
import org.spigotmc.lidle.raidwars.invites.PartyInvite;
import org.spigotmc.lidle.utils.generics.Action;
import org.spigotmc.lidle.utils.invites.InviteDispatcher;

public class PartyInviteActionEvent extends PartyEvent {

    private Action action;
    private InviteDispatcher inviteDispatcher;

    public PartyInviteActionEvent(FPlayer whoFired, PartyInvite partyInvite, Action action) {
        super(whoFired, EventType.INVITE_ACTION);
        this.action = action;
        this.inviteDispatcher = new InviteDispatcher(partyInvite, this.whoFired);
    }

    public Action getAction() {
        return action;
    }

    public InviteDispatcher getInviteDispatcher() {
        return inviteDispatcher;
    }
}
