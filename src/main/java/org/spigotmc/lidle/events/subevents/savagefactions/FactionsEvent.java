package org.spigotmc.lidle.events.subevents.savagefactions;

import com.massivecraft.factions.FPlayer;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.spigotmc.lidle.events.subevents.savagefactions.game.GameListener;
import org.spigotmc.lidle.events.subevents.savagefactions.party.PartyListener;
import org.spigotmc.lidle.events.subevents.savagefactions.wager.WagerListener;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;

public class FactionsEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private static boolean listenersRegistered = false;

    protected RaidPlayer whoFired;

    private boolean isCancelled = false;

    protected FactionsEvent(RaidPlayer whoFired) {
        this.whoFired = whoFired;
    }

    protected FactionsEvent(FPlayer whoFired) {
        this.whoFired = RaidPlayer.getOrCreateRaidPlayer(whoFired);
    }

    public RaidPlayer getPlayer() {
        return this.whoFired;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public static boolean registerListeners() {
        if(!listenersRegistered) {
            new GameListener();
            new PartyListener();
            new WagerListener();
            listenersRegistered = true;
        }

        return listenersRegistered;
    }

    protected static RaidPlayer getAsRaidPlayer(FPlayer fPlayer) {
        return RaidPlayer.getOrCreateRaidPlayer(fPlayer);
    }
}
