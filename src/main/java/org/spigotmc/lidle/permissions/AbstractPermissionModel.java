package org.spigotmc.lidle.permissions;

import org.bukkit.command.CommandSender;
import org.spigotmc.lidle.RaidWars;

/*Authored by: Matt Bessler 8/11/2018*/
abstract class AbstractPermissionModel {

    private final RaidWars pl;
    private final CommandSender sender;

    public AbstractPermissionModel(RaidWars pl, CommandSender sender){
        this.pl = pl;
        this.sender = sender;
    }

    protected RaidWars getPlugin(){
        return pl;
    }

    public CommandSender getSender() {
        return sender;
    }

    protected boolean hasPermission(String permission) {
        return pl.hasPermission(getSender(), "raidwars." + permission);
    }

}
