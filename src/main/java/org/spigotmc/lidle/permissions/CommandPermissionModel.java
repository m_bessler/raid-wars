package org.spigotmc.lidle.permissions;

import org.bukkit.command.CommandSender;
import org.spigotmc.lidle.RaidWars;

/*Authored by: Matt Bessler 8/11/2018*/
public class CommandPermissionModel extends AbstractPermissionModel {

    public CommandPermissionModel(RaidWars pl, CommandSender sender) {
        super(pl, sender);
    }

    public boolean mayUseSetChest() {
        return hasPermission(Node.SET_CHEST);
    }

    public boolean mayUseSetChestItem() {
        return hasPermission(Node.SET_CHEST_ITEM);
    }

    public boolean mayUseSetSpawn(){
        return hasPermission(Node.SET_SPAWN);
    }

    public static final class Node {
        public static final String SET_CHEST = "commands.setchest";
        public static final String SET_CHEST_ITEM = "commands.setchestitem";
        public static final String SET_SPAWN = "commands.setspawn";
    }

}
