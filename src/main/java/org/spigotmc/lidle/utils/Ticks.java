package org.spigotmc.lidle.utils;

/*Authored by: Matt Bessler 8/10/2018*/
public class Ticks {

    private long ticks;

    public Ticks(){
        this.ticks = 0;
    }

    public Ticks(long ticks) {
        this.ticks = ticks;
    }

    public Ticks(int ticks) {
        this.ticks = ticks;
    }

    public void setTicks(long ticks) {
        this.ticks = ticks;
    }

    public long getTicks(){
        return ticks;
    }

    public void setTicksAsSeconds(long seconds) {
        this.ticks = 20L * seconds;
    }

    public long getTicksAsSeconds(){
        return ticks / 20L;
    }

    public void setTicksAsMinutes(long minutes) {
        this.ticks = 20L * 60 * minutes;
    }

    public long getTicksAsMinutes(){
        return getTicksAsSeconds() / 60L;
    }

    public void setTicksAsHours(long hours) {
        this.ticks = 20L * 60 * 60 * hours;
    }

    public long getTicksAsHours() {
        return getTicksAsMinutes() / 60L;
    }

}
