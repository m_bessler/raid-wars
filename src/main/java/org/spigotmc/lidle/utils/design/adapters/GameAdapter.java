package org.spigotmc.lidle.utils.design.adapters;

import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.utils.generics.State;

interface GameAdapter {

    /**
     * @return the current team for the party if it exists | null otherwise
     * */
    Team getTeamForParty();

    /**
     * @return the opposing team for the party in the observableGame if both exist | null otherwise
     * */
    Team getOpposingTeamFor(Team team);

    /**
     * @return {@link State#ALLOW} if the requirements are met to start the observableGame | {@link State#DENY} otherwise
     * */
    State getStartGameState();

    /**
     * @return true if the team has enough players to start the game | false otherwise
     * */
    boolean doesTeamHaveEnoughMembers();

}
