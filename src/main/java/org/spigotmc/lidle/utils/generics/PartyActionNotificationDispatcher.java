package org.spigotmc.lidle.utils.generics;

import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.Messageable;

public interface PartyActionNotificationDispatcher<T extends Messageable> extends NotificationDispatcher {

    /**
     * Notifies the party a player has joined them
     * */
    void dispatchPlayerJoinedPartyAction();

    /**
     * Notifies the party a player has left them
     * */
    void dispatchPlayerLeftPartyAction();

    /**
     * Notifies the party a player has been kicked
     * */
    void dispatchKickMemberAction();

    /**
     * Notifies the party their party has been disbanded
     * */
    void dispatchDisbandPartyAction();

    /**
     * Notifies the leader of the party
     * */
    void dispatchPartyLeaderNotification(Message message);

    /**
     * Notifies all members of the party
     * */
    void dispatchPartyNotification(Message message);

    /**
     * Notifies the person who invoked an action
     * */
    void dispatchNotificationToWhoInvoked(Message message);

    /**
     * Notifies the person whom the action was invoked on
     * */
    void dispatchNotificationToMemberInvokedOn(Message message, T who);

    /**
     * Dispatches a notification to the person who invoked the action by default
     * */
    default void dispatchNotification(Message message) {
        dispatchNotificationToWhoInvoked(message);
    }

}
