package org.spigotmc.lidle.utils.generics;

import org.spigotmc.lidle.utils.messaging.Message;

public interface NotificationDispatcher {

    void dispatchNotification(Message message);

}
