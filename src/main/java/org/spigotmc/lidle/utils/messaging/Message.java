package org.spigotmc.lidle.utils.messaging;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

/*Authored by: Matt Bessler 8/11/2018*/
public class Message {

    private String message;

    public Message(String message) {
        this.message = message;
    }

    public void broadcastMessage() {
        Messaging.broadcastMessage(message);
    }

    public void sendMessage(CommandSender sender) {
        Messaging.sendMessage(sender, message);
    }

    public static void sendTitle(Player player, Message title, Message subTitle, int fadeIn, int stay, int fadeOut) {
        Messaging.sendTitle(player, title.toString(), subTitle.toString(), fadeIn, stay, fadeOut);
    }

    public static void sendTitle(Player player, String title, String subTitle, int fadeIn, int stay, int fadeOut) {
        Messaging.sendTitle(player, title, subTitle, fadeIn, stay, fadeOut);
    }

    public static final class MessageBuilder {

        private String message;
        private MessageType messageType;
        private boolean withPrefix = false;

        private MessageBuilder(){
            message = "";
            messageType = null;
        }

        public static MessageBuilder builder() {
            return new MessageBuilder();
        }

        public MessageBuilder withMessage(String... message) {
            return withMessage(Arrays.asList(message));
        }

        public MessageBuilder withMessage(List<String> message) {
            return withMessage(Messaging.formatMessageToString(message));
        }

        public MessageBuilder withMessage(String message) {
            if(this.message.isEmpty()) {
                this.message = message;
            } else {
                this.message = "\n" + message;
            }
            return this;
        }

        public MessageBuilder withMessageType(MessageType messageType) {
            this.messageType = messageType;
            return this;
        }

        public MessageBuilder withServerPrefix(boolean withPrefix) {
            this.withPrefix = withPrefix;
            return this;
        }

        public Message build() {
            StringBuilder builder = new StringBuilder();

            if(withPrefix) {
                builder.append("")
                        .append(" ");
            }

            if(messageType != null) {
                message = messageType.formatMessage(message);
            }

            builder.append(message);
            String finalMessage = builder.toString();
            return new Message(finalMessage);
        }

    }

}
