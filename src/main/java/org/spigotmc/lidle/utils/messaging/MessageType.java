package org.spigotmc.lidle.utils.messaging;/*Authored by: Matt Bessler 8/11/2018*/

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public enum MessageType {

    DEFAULT(ChatColor.WHITE),
    SUCCESS(ChatColor.GREEN),
    ERROR(ChatColor.RED, "Error:", "!"),
    INFO(ChatColor.AQUA),
    WARNING(ChatColor.RED, "", "!");

    private ChatColor color;
    private String colorCode;
    private String prefix = "";
    private String suffix = "";

    MessageType(ChatColor color) {
        this(color, "", "");
    }

    MessageType(ChatColor color, String prefix) {
        this(color, prefix, "");
    }

    MessageType(String prefix, String suffix) {
        this(ChatColor.WHITE, prefix, suffix);
    }

    MessageType(ChatColor color, String prefix, String suffix) {
        this.color = color;
        this.colorCode = "&" + color.getChar();
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public String formatMessage(String message) {
        StringBuilder builder = new StringBuilder();

        String reset = "§" + ChatColor.RESET.getChar();
        if(message.contains(reset)) {
            String code = "§" + color.getChar();
            message = message.replaceAll(reset, code);
        }

        builder.append(color);
        if(!prefix.isEmpty()) builder.append(prefix + " ");
        builder.append(message);
        if(!suffix.isEmpty()) builder.append(suffix);

        return builder.toString();
    }

}
