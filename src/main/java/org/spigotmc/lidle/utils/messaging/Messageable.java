package org.spigotmc.lidle.utils.messaging;

public interface Messageable {

    void sendMessage(Message message);

    default void sendTitle(String title) {
        Message titleMessage = new Message(title);
        sendTitle(titleMessage, new Message(""));
    }

    default void sendTitle(Message title) {
        sendTitle(title.toString());
    }

    default void sendTitle(String title, String subTitle) {
        Message titleMessage = new Message(title);
        Message subTitleMessage = new Message(subTitle);
        sendTitle(titleMessage, subTitleMessage, 2, 5, 2);
    }

    default void sendTitle(Message title, Message subTitle) {
        sendTitle(title, subTitle, 2, 5, 2);
    }

    default void sendTitle(String title, String subTitle, int fadeIn, int stay, int fadeOut) {
        Message titleMessage = new Message(title);
        Message subTitleMessage = new Message(subTitle);
        sendTitle(titleMessage, subTitleMessage, fadeIn, stay, fadeOut);
    }

    void sendTitle(Message title, Message subTitle, int fadeIn, int stay, int fadeOut);

    default void sendMessage(String msg) {
        Message message = Message.MessageBuilder.builder()
                .withMessage(msg)
                .withMessageType(MessageType.DEFAULT)
                .build();

        sendMessage(message);
    }

}
