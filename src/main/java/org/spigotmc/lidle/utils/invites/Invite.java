package org.spigotmc.lidle.utils.invites;

import org.spigotmc.lidle.utils.generics.Named;

interface Invite extends Named {

    /**
     * Denies the invite by the invited entity
     * @return true if the invite was denied | false otherwise
     * */
    boolean deny();

    /**
     * Accepts the invite by the invited entity
     * @return true if the invite was accepted | false otherwise
     * */
    boolean accept();

    /**
     * @return true if the invite was accepted | false otherwise
     * */
    boolean isAccepted();

    /**
     * @return true if this invite still exists | false otherwise
     * */
    boolean doesExist();

    /**
     * The name of the invite
     * */
    String getInviteName();

    /**
     * The name of the invite
     * */
    default String getName() {
        return getInviteName();
    }

}
