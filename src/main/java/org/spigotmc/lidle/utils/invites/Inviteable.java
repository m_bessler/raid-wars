package org.spigotmc.lidle.utils.invites;

import org.spigotmc.lidle.utils.generics.Named;
import org.spigotmc.lidle.utils.messaging.Messageable;

public interface Inviteable<T extends AbstractInvite> extends Named, Messageable {

    /**
     * Sends an invite to some object
     * */
    void invite(T invite);

    /**
     * @return the invite that was sent to this object | null if no invite exists
     * */
    T getInvite();

    /**
     * @return true if this object currently has an existing invite | false otherwise
     * */
    default boolean hasInvite() {
        return getInvite() != null;
    }

    /**
     * Accepts the invite that was sent to the object
     * */
    void acceptInvite();

    /**
     * Denies the invite that was sent to the object
     * */
    void denyInvite();

    /**
     * Revokes the current invite sent to the object
     * */
    default void revokeInvite() {
        denyInvite();
    }

}
