package org.spigotmc.lidle.utils.invites;

import org.spigotmc.lidle.raidwars.invites.InviteType;
import org.spigotmc.lidle.utils.generics.Named;

public abstract class AbstractInvite<T extends Inviteable> implements Invite {

    private InviteType inviteType;
    private T whoInvited;
    private boolean accepted;
    private boolean exists;

    public AbstractInvite(T whoInvited, InviteType inviteType) {
        this.whoInvited = whoInvited;
        this.inviteType = inviteType;
        this.accepted = false;
        this.exists = true;
    }

    /**
     * @return the object which invoked the invite you
     * */
    public T getWhoInvited() {
        return whoInvited;
    }

    public InviteType getInviteType() {
        return inviteType;
    }

    /**
     * @return the name of the object which whoInvited you
     * */
    public String getName() {
        return whoInvited.getName();
    }

    /**
     * Set whether or not this invite still exists
     * @return true if this invite still exists | false otherwise
     * */
    protected boolean doesExist(boolean exists) {
        this.exists = exists;
        return doesExist();
    }

    @Override
    public boolean doesExist() {
        return exists;
    }

    /**
     * Set whether or not this invite has been accepted
     * @return true if this invite was accepted | false otherwise
     * */
    protected boolean isAccepted(boolean accepted) {
        this.accepted = accepted;
        return isAccepted();
    }

    @Override
    public boolean isAccepted() {
        return accepted;
    }


}
