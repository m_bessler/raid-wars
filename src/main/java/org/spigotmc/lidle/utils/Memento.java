package org.spigotmc.lidle.utils;

public class Memento<T> {

    private T state;

    public Memento(T state) {
        this.state = state;
    }

    public T getState() {
        return state;
    }

    public void setState(T state) {
        this.state = state;
    }

    public Memento<T> save() {
        return new Memento<>(state);
    }

    public void restore(Memento<T> memento) {
        this.state = memento.getState();
    }

}
