package org.spigotmc.lidle.utils.fakes.raidwars;

import com.massivecraft.factions.Faction;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;

public class FakeParty extends Party {

    private FakeParty(Faction faction, RaidPlayer partyLeader) {
        super(faction, new FakeRaidPlayer(new FakePlayer()));
    }


}
