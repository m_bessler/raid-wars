package org.spigotmc.lidle.utils.fakes.raidwars;

import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.team.Team;

public class FakeTeam extends Team {

    public FakeTeam(Party party) {
        super(party);
    }

}
