package org.spigotmc.lidle.utils.fakes.raidwars;

import org.bukkit.entity.Player;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.utils.fakes.raidwars.FakePlayer;

public class FakeRaidPlayer extends RaidPlayer {

    private FakePlayer fakePlayer;

    public FakeRaidPlayer(FakePlayer fakePlayer) {
        super(fakePlayer);
        this.fakePlayer = fakePlayer;
    }

    @Override
    public Player getPlayer() {
        return fakePlayer;
    }

    @Override
    public String getName() {
        return fakePlayer.getName();
    }

}
