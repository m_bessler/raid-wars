package org.spigotmc.lidle.utils.factions;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import org.bukkit.entity.Player;

import java.util.UUID;

public final class FProxy {


    public static FPlayer getAsFPlayer(Player p) {
        UUID uuid = p.getUniqueId();
        for (FPlayer fplayer : FPlayers.getInstance().getAllFPlayers()) {
            if (fplayer.getPlayer().getUniqueId().equals(uuid)) {
                return fplayer;
            }
        }

        return null;
    }

    public static UUID getUUID(FPlayer fPlayer) {
        return fPlayer.getPlayer().getUniqueId();
    }

}
