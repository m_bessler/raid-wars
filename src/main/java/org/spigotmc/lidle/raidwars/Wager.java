package org.spigotmc.lidle.raidwars;

import org.bukkit.inventory.ItemStack;
import org.spigotmc.lidle.raidwars.team.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Wager {

    private Map<Team, List<ItemStack>> team1WagerItems = new HashMap<>();
    private Map<Team, List<ItemStack>> team2WagerItems = new HashMap<>();

    public Wager(Team team1, Team team2) {
        team1WagerItems.put(team1, new ArrayList<>());
        team2WagerItems.put(team2, new ArrayList<>());
    }

    public void startWager() {

    }

    public void finalizeWager() {

    }

}
