package org.spigotmc.lidle.raidwars.party.scoreboard;

import org.bukkit.ChatColor;
import org.spigotmc.lidle.utils.messaging.Messages;

import java.util.ArrayList;
import java.util.List;

public class ScoreboardEntries {

    private List<Entry> entries = new ArrayList<>();

    /**
     * The amount of blank entries currently in the {@link #entries} list
     * */
    private int uniqueEntries = 0;

    public void addLineEntry(String str) {
        String color = ChatColor.getLastColors(str);
        str = ChatColor.stripColor(str);

        String entry = Messages.recursive(25, "", str);
        entry = color + entry + getUniqueEntry(uniqueEntries);

        Entry lineEntry = new Entry("line_" + String.valueOf(entries.size()), entry);
        addEntry(lineEntry);
    }

    public void addBlankEntry() {
        String entry = getUniqueEntry(uniqueEntries);
        Entry blankEntry = new Entry("blank_" + String.valueOf(entries.size()), entry);
        addEntry(blankEntry);
    }

    public void addEntry(Entry entry) {
        entries.add(entry);
    }

    public List<Entry> getEntries() {
        return entries;
    }

    private String getUniqueEntry(int uniqueEntries) {
        ChatColor colors[] = ChatColor.values();
        int chatColorsSize = colors.length;
        int colorIndex;

        //Determines if we need to add in an extra ChatColor to keep the entry unique. This happens when we
        //used up all the current ChatColors for this Scoreboard
        int extraIterations = 0;
        if(uniqueEntries >= chatColorsSize) {
            int diff = uniqueEntries - chatColorsSize;
            while(diff >= chatColorsSize) {
                diff = diff - chatColorsSize;
                extraIterations++;
            }
            extraIterations++;
            colorIndex = diff;
        } else {
            colorIndex = chatColorsSize - (chatColorsSize - uniqueEntries);
        }

        int iterations = 1 + extraIterations;
        String entry = "";
        for(int i = 0; i < iterations; i++) {
            int index = colorIndex + i;
            if(index == 21) {
                index = 0;
            }
            entry += colors[index];
        }

        this.uniqueEntries++;
        return entry;
    }
}
