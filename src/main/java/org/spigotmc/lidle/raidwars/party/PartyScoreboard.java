package org.spigotmc.lidle.raidwars.party;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableObjectValue;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;
import org.spigotmc.lidle.raidwars.game.GameHelper;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.party.scoreboard.Entry;
import org.spigotmc.lidle.raidwars.party.scoreboard.ScoreboardEntries;
import org.spigotmc.lidle.utils.BukkitUtil;

import java.util.*;

public class PartyScoreboard implements Observer {

    // TODO: 8/22/2018 Refactor this to use the ScoreboardEntriesProxy class

    private static String titleFormat = ChatColor.GREEN + "%faction%'s" +
                                    ChatColor.GRAY + " RaidWars Party";

    private Scoreboard board;
    private Objective objective;
    private ScoreboardEntries entries;
    private Set<RaidPlayer> activeScoreboards = new HashSet<>();
    private GameHelper gameHelper;

    PartyScoreboard(Party party) {
        this.board = getNewScoreboard();
        this.gameHelper = new GameHelper(party);
        this.objective = board.registerNewObjective("ServerName", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        buildScoreboardForParty(party);
    }

    private void buildScoreboardForParty(Party party) {
        //Scoreboard header/title
        String displayName = titleFormat.replaceAll("%faction%", party.getTeamName());
        objective.setDisplayName(displayName);

        updatePartiesScoreboard(party);
    }

    private void updatePartiesScoreboard(final Party party) {
        Set<RaidPlayer> partyMembers = party.getPartyMembers();
        partyMembers.forEach(partyMember -> {
            Player player = partyMember.getPlayer();
            updateScoreboard(party, player);
        });
    }

    private void updateScoreboard(Party party, Player player) {
        entries = new ScoreboardEntries();
        activeScoreboards = new HashSet<>();
        activeScoreboards.addAll(party.getPartyMembers());

        entries.addLineEntry(ChatColor.GRAY + "_");
        entries.addBlankEntry();
        addFactionEntry(party.getTeamName());
        addPartyLeaderEntry(party.getPartyLeader().getPlayer());
        entries.addBlankEntry();
        addPartyMembersEntry(party.getPartyMembers());
        entries.addBlankEntry();
        addGameEntry(party);

        registerEntries();
        player.setScoreboard(board);
    }

    private void removePartiesActiveScoreboards(Set<RaidPlayer> partyMembers) {
        partyMembers.forEach(partyMember -> {
            Player player = partyMember.getPlayer();
            removeActiveScoreboard(player);
        });
    }

    private void removeActiveScoreboard(Player player) {
        Scoreboard board = getNewScoreboard();
        player.setScoreboard(board);
    }

    private void refreshPartiesScoreboard(final Party party) {
        Set<RaidPlayer> partyMembers = party.getPartyMembers();
        partyMembers.forEach(partyMember -> {
            Player player = partyMember.getPlayer();
            refreshScoreboard(player);
        });
    }

    private void refreshScoreboard(Player player) {
        player.setScoreboard(board);
    }

    private void insertEntry(int score, Entry entry) {
        //Inserting entries based on their score and not index
        for(String registeredEntry : board.getEntries()) {
            Score entryScore = objective.getScore(registeredEntry);
            if(entryScore.getScore() >= score) {
                entryScore.setScore(entryScore.getScore() + 1);
            }
        }

        registerEntry(score, entry);
    }

    private void registerEntries() {
        List<Entry> entries = this.entries.getEntries();

        for(int i = 0; i < entries.size(); i++) {
            Entry entry = entries.get(i);
            registerEntry(entries.size() - i, entry);
        }
    }

    private void registerEntry(int score, Entry entry) {
        if(!entryExists(entry)) {
            entries.addEntry(entry);
        }

        addEntry(score, entry.getName(), entry.getEntry(), entry.getSuffix(), entry.getPrefix());
    }

    private void addEntry(int score, String name, String entry, String suffix, String prefix) {
        Team team = board.registerNewTeam(buildTeamName(name, score));

        team.setPrefix(prefix);
        team.setSuffix(ChatColor.GOLD + suffix);
        team.addEntry(entry);

        objective.getScore(entry).setScore(score);
    }

    private void unregisterEntry(Entry entry) {
        int index = entries.getEntries().indexOf(entry);

        if(entries.getEntries().contains(entry)) {
            entries.getEntries().remove(entry);
        }

        removeEntry(index, entry.getName(), entry.getEntry());
    }

    private void removeEntry(int score, String name, String entry) {
        String teamName = buildTeamName(name, score);
        Team team = board.getTeam(teamName);
        team.removeEntry(entry);
    }

    private boolean entryExists(Entry entry) {
        if(entry.getName().contains("blank") || entry.getName().contains("line")) {
            return true;
        }

        String entryName = ChatColor.stripColor(entry.getName());
        for(Entry scoreboardEntry : entries.getEntries()) {
            String name = scoreboardEntry.getName();
            name = ChatColor.stripColor(name);
            if(name.equalsIgnoreCase(entryName)) {
                return true;
            }
        }

        return false;
    }

    private String buildTeamName(String name, int score) {
        return name + "_" + String.valueOf(score);
    }

    private Scoreboard getNewScoreboard() {
        return Bukkit.getScoreboardManager().getNewScoreboard();
    }

    private void addPartyLeaderEntry(Player partyLeader) {
        Entry partyLeaderEntry = buildPartyLeaderEntry(partyLeader);
        entries.addEntry(partyLeaderEntry);
    }

    private Entry buildPartyLeaderEntry(Player player) {
        String partyLeaderName = player.getDisplayName();
        Entry partyLeaderEntry = new Entry("partyLeader", ChatColor.GRAY + "» Party Leader: ", ChatColor.AQUA + partyLeaderName);
        return partyLeaderEntry;
    }

    private void addFactionEntry(String factionTag) {
        Entry factionEntry = buildFactionEntry(factionTag);
        entries.addEntry(factionEntry);
    }

    private Entry buildFactionEntry(String factionTag) {
        Entry factionNameEntry = new Entry("factionName", ChatColor.GRAY + "» Faction: ", ChatColor.GREEN + factionTag);
        return factionNameEntry;
    }

    private void updatePartyMembersEntry(RaidPlayer raidPlayer) {
        Player player = raidPlayer.getPlayer();

        //Removing player from scoreboard
        if(activeScoreboards.contains(raidPlayer)) {
            Entry entry = getExistingEntryForPlayer(player);
            unregisterEntry(entry);
        } else {
            //The next number the player should be tagged with in the party
            int playerNumber = getLastPartyMemberNumber();

            //Builds visual entry with the player's number tagged to it
            Entry entry = buildPartyMemberEntry(playerNumber, player);

            int score = getLastPartyMemberScore();
            insertEntry(score, entry);
        }
    }

    private void addPartyMembersEntry(Set<RaidPlayer> partyMembers) {
        Entry partyMembersEntry = new Entry("partyMembers", ChatColor.GRAY + "» Party Members: ");
        entries.addEntry(partyMembersEntry);
        entries.addBlankEntry();
        int index = 0;
        for(RaidPlayer raidPlayer : partyMembers) {
            Player player = raidPlayer.getPlayer();
            Entry partyMemberEntry = buildPartyMemberEntry(index, player);
            entries.addEntry(partyMemberEntry);
            index++;
        }
    }

    private Entry buildPartyMemberEntry(int index, Player player) {
        String name = "partyMember_" + String.valueOf(index);
        String playerName = player.getDisplayName();
        String prefix = ChatColor.GRAY + "  " + String.valueOf(index + 1) + ".";
        String suffix = "";
        String entry = ChatColor.GREEN + "  " + playerName;
        if(index == 0) {
            suffix = ChatColor.GRAY + " (Party Leader)";
        }
        Entry partyMemberEntry = new Entry(name, entry, suffix, prefix);

        return partyMemberEntry;
    }

    private Entry getExistingEntryForPlayer(Player player) {
        String displayName = ChatColor.stripColor(player.getDisplayName());

        for(Entry entry : entries.getEntries()) {
            String entryName = ChatColor.stripColor(entry.getName());
            Bukkit.broadcastMessage(entry.getName());
            if(entryName.equals(displayName)) {
                return entry;
            }
        }

        return null;
    }

    /**
     * @return the index of the last party member in the {@link #entries} list
     * */
    private int getLastPartyMemberScore() {
        int score = 0;
        for(Entry entry : entries.getEntries()) {
            if(entry.getName().contains("partyMember_")){
                score = objective.getScore(entry.getEntry()).getScore();
            }
        }

        return score;
    }

    private int getLastPartyMemberNumber() {
        int index = 0;
        for(Entry entry : entries.getEntries()) {
            if(entry.getName().contains("partyMember_")){
                index++;
            }
        }

        return index;
    }

    private void addGameEntry(Party party) {
        Entry gameEntry = buildGameEntry();
        entries.addEntry(gameEntry);
    }

    private Entry buildGameEntry() {
        String opponent = BukkitUtil.coloredMessage(ChatColor.RED, "None");
        if(gameHelper.getTeamForParty() != null && gameHelper.getGameForParty() != null) {
            opponent = BukkitUtil.coloredMessage(ChatColor.LIGHT_PURPLE, gameHelper.getOpposingTeamForParty().getTeamName());
        }

        Entry gameEntry = new Entry("gameOpponent", gameHelper.getOpposingTeamNameForParty(), "", ChatColor.GRAY + "» Opponent: ");
        return gameEntry;
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof Party) {
            Party party = (Party) o;
            //If a player has been added or removed, refresh the scoreboard
            if(arg instanceof RaidPlayer) {
                updatePartyMembersEntry((RaidPlayer) arg);
                refreshPartiesScoreboard(party);
            } else if(arg instanceof Team) {

            }
            else {
                //If the party has been disbanded remove the active scoreboard from the party members
                if(party.getPartyMembers() == null || party.getPartyMembers().isEmpty()) {
                    removePartiesActiveScoreboards(activeScoreboards);
                } else {
                    updatePartiesScoreboard(party);
                }
            }
        }
    }
}
