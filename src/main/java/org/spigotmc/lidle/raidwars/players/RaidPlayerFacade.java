package org.spigotmc.lidle.raidwars.players;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.zcore.persist.MemoryFPlayer;
import com.massivecraft.factions.zcore.persist.json.JSONFPlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Facilitates communication between {@link RaidPlayer} class and the
 * {@link MemoryFPlayer} class
 * */
class RaidPlayerFacade extends JSONFPlayer {

    private FPlayer fPlayer;

    RaidPlayerFacade(){super(UUID.randomUUID().toString());}

    public RaidPlayerFacade(Player player) {
        this(getPlayerAsFPlayer(player));
    }

    public RaidPlayerFacade(UUID uuid) {
        this(getUUIDAsFPlayer(uuid));
    }

    public RaidPlayerFacade(FPlayer fPlayer) {
        super((MemoryFPlayer) fPlayer);
        this.fPlayer = fPlayer;
    }

    public UUID getUUID() {
        return UUID.fromString(getId());
    }

    private static FPlayer getPlayerAsFPlayer(Player player) {
        FPlayer fPlayer = FPlayers.getInstance().getByPlayer(player);
        return fPlayer;
    }

    private static FPlayer getUUIDAsFPlayer(UUID uuid) {
        FPlayer fPlayer = FPlayers.getInstance().getById(uuid.toString());
        return fPlayer;
    }

}
