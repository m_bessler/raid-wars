package org.spigotmc.lidle.raidwars.players;

import com.massivecraft.factions.FPlayer;
import org.bukkit.entity.Player;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.invites.PartyInvite;
import org.spigotmc.lidle.utils.factions.FProxy;
import org.spigotmc.lidle.utils.fakes.raidwars.FakePlayer;
import org.spigotmc.lidle.utils.generics.Named;
import org.spigotmc.lidle.utils.invites.Inviteable;
import org.spigotmc.lidle.utils.messaging.Message;
import org.spigotmc.lidle.utils.messaging.MessageType;
import org.spigotmc.lidle.utils.messaging.Messageable;
import org.spigotmc.lidle.utils.messaging.Messages;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Represents a player that wants to play in a RaidWars game.  This is a type of {@link FPlayer} and
 * communication between the two classes is facilitated through the {@link RaidPlayerFacade} class.
 * */
public class RaidPlayer extends RaidPlayerFacade implements Inviteable<PartyInvite>, Messageable, Named {

    private static Map<UUID, RaidPlayer> raidPlayerMap = new HashMap<>();

    private Party party;
    private PartyInvite invite;
    private boolean inParty;
    private boolean inGame;

    protected RaidPlayer(FakePlayer fakePlayer){
        super();
    }

    private RaidPlayer(FPlayer fPlayer) {
        super(fPlayer);
    }

    private RaidPlayer(Player player) {
        super(player);
    }

    private RaidPlayer(UUID uuid) {
        super(uuid);
    }

    public boolean isInGame() {
        return inGame;
    }

    public void setParty(Party party) {
        if(party != null) {
            this.party = party;
            inParty = true;
        } else {
            inParty = false;
        }
    }

    public Party getParty() {
        return party;
    }

    public boolean inParty() {
        return inParty;
    }

    @Override
    public void invite(PartyInvite invite) {
        this.invite = invite;

        String msg = Messages.YOU_HAVE_BEEN_INVITED_TO_PARTY;
        String playerName = invite.getInviteName();
        msg = msg.replaceAll("%player%", playerName);
        Message message = Message.MessageBuilder.builder()
                .withMessage(msg)
                .withMessageType(MessageType.SUCCESS)
                .build();

        sendMessage(message);
    }

    @Override
    public PartyInvite getInvite() {
        return invite;
    }

    @Override
    public void acceptInvite() {
        if(invite != null) {
            Party party = invite.getInvitedParty();
            party.addPartyMember(this);
            invite = null;
        }
    }

    @Override
    public void denyInvite() {
        invite = null;
    }

    @Override
    public void sendMessage(Message message) {
        message.sendMessage(getPlayer());
    }

    @Override
    public void sendTitle(Message title, Message subTitle, int fadeIn, int stay, int fadeOut) {
        Message.sendTitle(getPlayer(), title, subTitle, fadeIn, stay, fadeOut);
    }

    @Override
    public String getName() {
        return getPlayer().getName();
    }

    public static RaidPlayer getOrCreateRaidPlayer(FPlayer player) {
        UUID uuid = FProxy.getUUID(player);
        raidPlayerMap.putIfAbsent(uuid, new RaidPlayer(player));
        return raidPlayerMap.get(uuid);
    }

    public static RaidPlayer getOrCreateRaidPlayer(Player player) {
        UUID uuid = player.getUniqueId();
        raidPlayerMap.putIfAbsent(uuid, new RaidPlayer(player));
        return raidPlayerMap.get(uuid);
    }

    public static RaidPlayer getOrCreateRaidPlayer(UUID uuid) {
        raidPlayerMap.putIfAbsent(uuid, new RaidPlayer(uuid));
        return raidPlayerMap.get(uuid);
    }

}
