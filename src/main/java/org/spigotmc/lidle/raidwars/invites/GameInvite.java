package org.spigotmc.lidle.raidwars.invites;

import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.utils.generics.Named;
import org.spigotmc.lidle.utils.invites.AbstractInvite;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.utils.invites.InviteDispatcher;

/**
 * An {@link AbstractInvite} implementation which invites a team to a game
 * */
public class GameInvite extends AbstractInvite<Team> implements Named {

    private Game invitedGame;

    public GameInvite(Team teamWhoInvited, Game invitedGame) {
        super(teamWhoInvited, InviteType.GAME);
        this.invitedGame = invitedGame;
    }

    public Game getInvitedGame() {
        return invitedGame;
    }

    @Override
    public boolean accept() {
        isAccepted(true);

        if(invitedGame != null && doesExist()) {
            doesExist(false);
            return true;
        }

        return false;
    }

    @Override
    public String getInviteName() {
        return getName();
    }

    @Override
    public boolean deny() {
        isAccepted(false);
        doesExist(false);

        return isAccepted();
    }
}
