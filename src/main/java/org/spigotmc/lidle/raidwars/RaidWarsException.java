package org.spigotmc.lidle.raidwars;

public class RaidWarsException extends Exception {

    RaidWarsException() {
        this("An exception was encountered!");
    }

    RaidWarsException(String msg) {
        super(msg);
    }

}
