package org.spigotmc.lidle.raidwars.team;

import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.utils.invites.Inviteable;
import org.spigotmc.lidle.raidwars.invites.GameInvite;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.utils.messaging.Message;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Team implements Inviteable<GameInvite>, Teamable {

    //UUID is the party leader's uuid
    private static Map<UUID, Team> teamMap = new HashMap<>();

    private Party party;
    private Game game;
    private GameInvite invite;

    protected Team(Party party) {
        this.party = party;
        registerNewTeam();
    }

    protected Team(Team other) {
        this.party = other.party;
        this.game = other.game;
        this.invite = other.invite;
    }

    private void registerNewTeam() {
        UUID uuid = party.getPartyLeader().getUUID();
        teamMap.put(uuid, this);
    }

    /**
     * @return the current party for this team
     * */
    public Party getParty() {
        return party;
    }

    /**
     * @return the current members in this party
     * */
    public Set<RaidPlayer> getTeamMembers() {
        return party.getPartyMembers();
    }

    /**
     * @return the current game this team is in
     * */
    public Game getGame() {
        return game;
    }

    /**
     * @return the {@link UUID} of the party leader
     * */
    public UUID getTeamLeaderUUID() {
        return party.getPartyLeader().getUUID();
    }

    /**
     * @return the number of party members this team has
     * */
    public int teamSize() {
        return party.getPartyMembers().size();
    }

    @Override
    public void invite(GameInvite invite) {
        this.invite = invite;
    }

    @Override
    public GameInvite getInvite() {
        return this.invite;
    }

    @Override
    public void acceptInvite() {
        if(invite != null && game == null) {
            this.game = invite.getInvitedGame();
            game.setTeam2(this);
            invite = null;
        }
    }

    @Override
    public void denyInvite() {
        invite = null;
    }

    @Override
    public String getTeamName() {
        return party.getTeamName();
    }

    @Override
    public String getLeaderName() {
        return party.getLeaderName();
    }

    @Override
    public void notifyMembers(Message msg) {
        party.notifyMembers(msg);
    }

    @Override
    public void sendTitle(Message title, Message subTitle, int fadeIn, int stay, int fadeOut) {
        party.sendTitle(title, subTitle, fadeIn, stay, fadeOut);
    }

    /**
     * @return the current team for this party if a team exists | null otherwise
     * */
    public static Team getOrRegisterTeamForParty(Party party) {
        RaidPlayer partyLeader = party.getPartyLeader();
        UUID uuid = partyLeader.getUUID();

        if(teamMap.get(uuid) == null) {
            registerTeamForParty(party);
        }

        return teamMap.get(uuid);
    }

    private static Team registerTeamForParty(Party party) {
        return new Team(party);
    }

    /**
     * @return true if a team exists for the current party | false otherwise
     * */
    public static boolean doesTeamExistForParty(Party party) {
        return getOrRegisterTeamForParty(party) != null;
    }

}
