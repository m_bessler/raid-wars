package org.spigotmc.lidle.raidwars.game;

import org.spigotmc.lidle.utils.generics.Dispatcher;
import org.spigotmc.lidle.utils.generics.NotificationDispatcher;
import org.spigotmc.lidle.utils.messaging.Message;

public class GameNotificationDispatcher implements NotificationDispatcher {

    private Game game;

    public GameNotificationDispatcher(Game game) {
        this.game = game;
    }

    @Override
    public void dispatchNotification(Message message) {

    }

}
