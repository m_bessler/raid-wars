package org.spigotmc.lidle.raidwars.game;

import org.spigotmc.lidle.raidwars.Arena;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.raidwars.Wager;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Game {

    private Arena arena;
    private Wager wager;
    private Team team1;
    private Team team2;
    private Team winningTeam;

    public Game(Team team1) {
        this.team1 = team1;
    }

    protected Game(Game other) {
        this.arena = other.arena;
        this.wager = other.wager;
        this.team1 = other.team1;
        this.team2 = other.team2;
        this.winningTeam = other.winningTeam;
    }

    public Team getTeam1() {
        return team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public Team getOpposingTeamForTeam(Team team) {
        UUID teamLeaderUUID = team.getTeamLeaderUUID();

        if(team.getTeamLeaderUUID().equals(teamLeaderUUID)) {
            return this.team1;
        } else {
            return team2;
        }
    }

    public Set<RaidPlayer> getPlayers() {
        Set<RaidPlayer> players = new HashSet<>();
        players.addAll(team1.getTeamMembers());
        players.addAll(team2.getTeamMembers());

        return players;
    }

    public void setWinner(Team winningTeam) {
        this.winningTeam = winningTeam;
    }

    public Team getWinner() {
        return winningTeam;
    }


}
