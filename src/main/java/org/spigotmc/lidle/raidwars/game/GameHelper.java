package org.spigotmc.lidle.raidwars.game;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.spigotmc.lidle.PluginConf;
import org.spigotmc.lidle.raidwars.party.Party;
import org.spigotmc.lidle.raidwars.team.Team;
import org.spigotmc.lidle.utils.BukkitUtil;
import org.spigotmc.lidle.utils.generics.State;

public class GameHelper {

    private static int minTeamSize = PluginConf.inst().getMinPartySize();
    private static int maxTeamSize = PluginConf.inst().getMaxPartySize();

    private Team team;
    private Party party;

    public GameHelper(Party party) {
        this.party = party;
        this.team = Team.getOrRegisterTeamForParty(party);
    }

    public GameHelper(GameHelper other) {
        this.party = other.party;
        this.team = other.team;
    }

    /**
     * @return the current team for the party if it exists | null otherwise
     * */
    public Team getTeamForParty() {
        return team;
    }

    /**
     * @return the current observableGame the party is queued in if it exists | null otherwise
     * */
    public Game getGameForParty() {
        if(team.getGame() == null) {
            return null;
        }
        return team.getGame();
    }

    /**
     * @return the opposing team for the party in the observableGame if both exist | null otherwise
     * */
    public Team getOpposingTeamForParty() {
        Game game = getGameForParty();

        if(game == null || team == null) {
            return null;
        } else {
            return game.getOpposingTeamForTeam(team);
        }
    }

    public String getOpposingTeamNameForParty() {
        Game game = getGameForParty();
        String opposingTeam;
        if(game == null || team == null) {
            opposingTeam = BukkitUtil.coloredMessage(ChatColor.RED, "None");
        } else {
            opposingTeam = game.getOpposingTeamForTeam(team).getTeamName();
            opposingTeam = BukkitUtil.coloredMessage(ChatColor.LIGHT_PURPLE, opposingTeam);
        }

        return opposingTeam;
    }

    /**
     * @return {@link State#ALLOW} if the requirements are met to start the observableGame | {@link State#DENY} otherwise
     * */
    public State getStartGameState() {
        //Team size must meet the required threshold
        if(!doesTeamSizeMeetThreshold()) {
            return State.DENY;
        }

        //Game must exist
        if(getGameForParty() == null) {
            return State.DENY;
        }

        //Game opponent must exist
        if(getOpposingTeamForParty() == null) {
            return State.DENY;
        }

        return State.ALLOW;
    }

    public boolean doesTeamSizeMeetThreshold() {
        Team team = getTeamForParty();

        if(team == null) {
            return false;
        }

        return team.teamSize() >= minTeamSize && team.teamSize() <= maxTeamSize;
    }

}
