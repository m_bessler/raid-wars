package org.spigotmc.lidle.tasks;

import org.spigotmc.lidle.RaidWars;

public abstract class BuildableExecutorConfig {

    protected RaidWars pl = RaidWars.inst();
    protected TaskExecutorConfig config;

    protected void setTaskExecutorConfig(TaskExecutorConfig config) {
        this.config = config;
    }

    protected abstract TaskExecutorConfig buildExecutionConfig();

}
