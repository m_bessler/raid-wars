package org.spigotmc.lidle.tasks;

import org.spigotmc.lidle.tasks.runtypes.DelayedTask;
import org.spigotmc.lidle.tasks.runtypes.ImmediateTask;
import org.spigotmc.lidle.tasks.runtypes.RepeatingTask;
import org.spigotmc.lidle.tasks.RunType.Type;

public class TaskExecutorConfig {

    public enum TaskType {
        ASYNC,
        SYNC
    }

    private long delay;
    private long period;
    private TaskType taskType;

    TaskExecutorConfig(RunType runType, TaskType taskType) {
        this.delay = runType.getDelay();
        this.period = runType.getPeriod();
        this.taskType = taskType;
    }

    public long getDelay() {
        return delay;
    }

    public long getPeriod() {
        return period;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private TaskType taskType;
        private RunConfiguration runConfiguration;

        Builder() {
            taskType = TaskType.SYNC;
            runConfiguration = new RunConfiguration();
            runConfiguration.setPeriod(new ImmediateTask());
        }

        public Builder withTaskType(TaskType taskType) {
            this.taskType = taskType;
            return this;
        }

        public Builder withRunType(Type runType) {
            return withRunType(runType, 0L);
        }

        public Builder withRunType(Type runType, long value) {
            if(runType == Type.DELAYED) {
                DelayedTask delay = new DelayedTask();
                delay.setDelay(value);
                runConfiguration.setDelay(delay);
            } else if(runType == Type.REPEATING) {
                RepeatingTask period = new RepeatingTask();
                period.setPeriod(value);
                runConfiguration.setPeriod(period);
            } else {
                ImmediateTask immediate = new ImmediateTask();
                immediate.setPeriod(value);
                runConfiguration.setPeriod(immediate);
            }

            return this;
        }

        public TaskExecutorConfig build() {
            return new TaskExecutorConfig(new RunType(runConfiguration), taskType);
        }
    }

}
