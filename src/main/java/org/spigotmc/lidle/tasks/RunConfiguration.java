package org.spigotmc.lidle.tasks;

import org.spigotmc.lidle.tasks.runtypes.Delay;
import org.spigotmc.lidle.tasks.runtypes.Repeat;

public class RunConfiguration {

    private long delay;
    private long period;

    public void setDelay(Delay delay) {
        this.delay = delay.getDelay();
    }

    public long getDelay() {
        return delay;
    }

    public void setPeriod(Repeat period) {
        this.period = period.getPeriod();
    }

    public long getPeriod() {
        return period;
    }

}
