package org.spigotmc.lidle.tasks;

import org.spigotmc.lidle.tasks.RunConfiguration;

public class RunType {

    public enum Type {
        DELAYED,
        REPEATING,
        IMMEDIATE
    }

    private long delay;
    private long period;

    public RunType(RunConfiguration runConfiguration) {
        this.delay = runConfiguration.getDelay();
        this.period = runConfiguration.getPeriod();
    }

    public long getDelay() {
        return delay;
    }

    public long getPeriod() {
        return period;
    }
}
