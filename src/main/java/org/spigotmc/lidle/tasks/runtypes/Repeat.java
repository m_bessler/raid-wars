package org.spigotmc.lidle.tasks.runtypes;

public interface Repeat extends RunValue<Long> {

    void setPeriod(long period);

    long getPeriod();

    default Long getValue() {
        return getPeriod();
    }

}
