package org.spigotmc.lidle.tasks.runtypes;

public interface RunValue<T> {

    T getValue();

}
