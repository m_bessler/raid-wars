package org.spigotmc.lidle.tasks.runtypes;


public interface Delay extends RunValue<Long> {

    void setDelay(long delay);

    long getDelay();

    default Long getValue() {
        return getDelay();
    }

}
