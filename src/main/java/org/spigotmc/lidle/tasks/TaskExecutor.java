package org.spigotmc.lidle.tasks;

public interface TaskExecutor {

    void executeTask();

}
