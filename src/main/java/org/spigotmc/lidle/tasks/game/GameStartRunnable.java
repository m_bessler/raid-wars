package org.spigotmc.lidle.tasks.game;

import org.bukkit.scheduler.BukkitRunnable;
import org.spigotmc.lidle.raidwars.game.Game;
import org.spigotmc.lidle.raidwars.players.RaidPlayer;
import org.spigotmc.lidle.raidwars.team.Team;

import java.util.Set;

class GameStartRunnable extends BukkitRunnable {

    private Game game;
    private Set<RaidPlayer> gamePlayers;
    private long countdown;

    public GameStartRunnable(Game game, long countdown) {
        this.game = game;
        this.gamePlayers = game.getPlayers();
        this.countdown = countdown;
    }

    @Override
    public void run() {
        if(countdown == 0) {
            gamePlayers.forEach(raidPlayer -> {
                raidPlayer.sendMessage("The game is starting!");
                // TODO: 8/25/2018 Send players to lobby
                cancel();
            });
        } else {
            gamePlayers.forEach(raidPlayer -> {
                if(countdown <= 10) {
                    raidPlayer.sendMessage("The game will start in " + countdown + "seconds!");
                } else if(countdown % 10 == 0) {
                    raidPlayer.sendMessage("The game will start in " + countdown + "seconds!");
                }
            });
            countdown--;
        }
    }
}
