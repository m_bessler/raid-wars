package org.spigotmc.lidle.tasks.game;

import org.bukkit.Bukkit;
import org.spigotmc.lidle.tasks.BuildableExecutorConfig;
import org.spigotmc.lidle.tasks.RunType;
import org.spigotmc.lidle.tasks.RunType.Type;
import org.spigotmc.lidle.tasks.TaskExecutorConfig;
import org.spigotmc.lidle.tasks.TaskExecutorConfig.TaskType;


import java.util.concurrent.Executor;

class GameExecutor extends BuildableExecutorConfig implements Executor {

    private long countdown;

    public GameExecutor(long countdown) {
        this.countdown = countdown;
        setTaskExecutorConfig(buildExecutionConfig());
    }

    @Override
    public void execute(Runnable command) {
        if(config.getTaskType() == TaskType.SYNC) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, command, 0L, 20L);
        } else if(config.getTaskType() == TaskType.ASYNC) {
            Bukkit.getScheduler().scheduleAsyncRepeatingTask(pl, command, 0L, 20L);
        }
    }

    @Override
    protected TaskExecutorConfig buildExecutionConfig() {
        return TaskExecutorConfig.builder()
                .withTaskType(TaskExecutorConfig.TaskType.SYNC)
                .withRunType(RunType.Type.DELAYED, countdown)
                .build();
    }
}
